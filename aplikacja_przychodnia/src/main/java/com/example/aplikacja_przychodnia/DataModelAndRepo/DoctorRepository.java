package com.example.aplikacja_przychodnia.DataModelAndRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    @Query("SELECT DISTINCT d.specialization FROM Doctor d")
    List<String> findAllSpecializations();

    @Query("SELECT DISTINCT d.user FROM Doctor d")
    List<User> findAllUsernames();

    DoctorDTO findAllBySpecialization(String specialization);

    DoctorDTO findByUserAndSpecialization(User user, String specialization);

    /*@Query("SELECT u.lastName, d.specialization FROM Doctor d\n" +
            "LEFT JOIN User u ON d.user.id = u.id")*/
    //List<FoundDoctors> findAllByLastNameAndSpecialization(String last_name, String specialization);

    @Transactional
    @Query(value = "SELECT \n" +
            "doctor.id, doctor.prof_title,doctor.specialization ,user.*\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id\n" +
            "AND doctor.specialization=:specialization ",nativeQuery = true)
    List<Doctor> findDoctorsBySpecialization(String specialization);

    @Transactional
    @Query(value = "SELECT \n" +
            "doctor.id, doctor.prof_title,doctor.specialization ,user.*\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id\n" +
            "AND user.last_name=:lastName",nativeQuery = true)
    List<Doctor> findDoctorsByLastName(String lastName);

    @Transactional
    @Query(value = "SELECT \n" +
            "doctor.id, doctor.prof_title,doctor.specialization ,user.*\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id\n",nativeQuery = true)
    List<Doctor> findAllDoctors();

    @Query(value = "select * from doctor where doctor.id=:doctorId",nativeQuery = true)
    Doctor findDoctorById(Long doctorId);

    @Query(value = "select doctor.id from doctor, user where doctor.user_id=user.user_id\n" +
            "AND user.email=:email",nativeQuery = true)
    Long findDoctorIdByEmail(String email);
}
