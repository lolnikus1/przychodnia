package com.example.aplikacja_przychodnia.DataModelAndRepo;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="visit_id")
    private Long id;

    private String description;

    @ManyToOne
    @JoinColumn(name="doctor_id", nullable = false)
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name="patient_id", nullable = false)
    private Patient patient;

    @Temporal(TemporalType.TIMESTAMP)
    private Date visitDateTime;

    private boolean isFinished;

    public Visit(){

    }

    public Visit(Long id, String description, Doctor doctor, Patient patient, Date visitDateTime, boolean isFinished) {
        this.id = id;
        this.description = description;
        this.doctor = doctor;
        this.patient = patient;
        this.visitDateTime = visitDateTime;
        this.isFinished = isFinished;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getVisitDateTime() {
        return visitDateTime;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public boolean isFinished() { return isFinished; }

    public void setFinished(boolean finished) { isFinished = finished; }

    public void setVisitDateTime(Date visitDateTime) {
        this.visitDateTime = visitDateTime;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }


}
