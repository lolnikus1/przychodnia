package com.example.aplikacja_przychodnia.DataModelAndRepo;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(  uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_id")
})
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;
    private String profTitle;
    private String specialization;

    @OneToMany(mappedBy = "doctor",fetch = FetchType.EAGER)
    private Set<Visit> visits;

    public Doctor() {
    }

    public Doctor(User user, String profTitle, String specialization) {
        this.user = user;
        this.profTitle = profTitle;
        this.specialization = specialization;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() { return user; }
    public void setUser(User user) { this.user = user; }

    public String getProfTitle() { return profTitle; }
    public void setProfTitle(String profTitle) { this.profTitle = profTitle; }

    public String getSpecialization() { return specialization; }
    public void setSpecialization(String specialization) { this.specialization = specialization; }

    public Set<Visit> getVisits() { return visits; }
    public void setVisits(Set<Visit> visits) { this.visits = visits; }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", user=" + user +
                ", profTitle='" + profTitle + '\'' +
                ", specialization='" + specialization + '\'' +
                ", visits=" + visits +
                '}';
    }
}
