package com.example.aplikacja_przychodnia.DataModelAndRepo;

public class DoctorDTO {
    private User user;
    private String specialization;

    public DoctorDTO(User user, String specialization){
        this.user = user;
        this.specialization = specialization;
    }



    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
