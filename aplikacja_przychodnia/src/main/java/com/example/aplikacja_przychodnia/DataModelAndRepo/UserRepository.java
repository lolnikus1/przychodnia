package com.example.aplikacja_przychodnia.DataModelAndRepo;


import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
    UserDTO findByLastName(String lastName);
}

