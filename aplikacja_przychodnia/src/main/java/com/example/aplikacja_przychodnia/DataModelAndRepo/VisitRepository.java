package com.example.aplikacja_przychodnia.DataModelAndRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface VisitRepository extends JpaRepository<Visit, Long> {

    List<Visit> getAllByDoctorAndVisitDateTime(Doctor doctor, Date visitDateTime);

    @Query(value = "SELECT * FROM przychodnia.visit " +
            "where patient_id=(SELECT id FROM przychodnia.patient where user_id= (select user_id from user where email=:email))\n" +
            "AND DATE(visit_date_time) >= current_date()\n" +
            "AND is_finished=0\n" +
            "ORDER BY visit_date_time",nativeQuery = true)
    List<Visit> findAllVisitsForUser(String email);

    @Query(value = "SELECT * FROM przychodnia.visit " +
            "where visit.doctor_id=(select id from doctor where user_id = (SELECT user_id FROM przychodnia.user WHERE email=:email))\n" +
            "AND DATE(visit_date_time) >= current_date()\n" +
            "AND is_finished=:isVisitFinished\n" +
            "ORDER BY visit_date_time",nativeQuery = true)
    List<Visit> findAllVisitsForDoctor(String email, boolean isVisitFinished);

    @Transactional
    @Modifying
    @Query(value = "UPDATE przychodnia.visit SET description=:description " +
            "WHERE ( visit_date_time=:visitDateTime AND doctor_id=:doctorId  AND patient_id=:patientId  );", nativeQuery = true)
    void addCommentToVisit(Date visitDateTime, Long doctorId, Long patientId, String description);

    @Transactional
    @Modifying
    @Query(value = "UPDATE przychodnia.visit SET is_finished=1  " +
            "WHERE ( visit_date_time=:visitDateTime AND doctor_id=:doctorId  AND patient_id=:patientId  );", nativeQuery = true)
    void addVisitAsPast(Date visitDateTime, Long doctorId, Long patientId);



}
