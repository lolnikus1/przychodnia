package com.example.aplikacja_przychodnia.navAndThemes;


import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.ui.views.MainPage;
import com.example.aplikacja_przychodnia.ui.views.tables.forDoctorView.EndedVisitsPerDoctor;
import com.example.aplikacja_przychodnia.ui.views.tables.forDoctorView.RegisteredVisitsPerDoctor;
import com.example.aplikacja_przychodnia.ui.views.tables.forPatientView.RegisteredVisitsPerPatient;
import com.example.aplikacja_przychodnia.ui.views.forms.FormFindDoctor;
import com.example.aplikacja_przychodnia.ui.views.forms.FormFindDoctorReceptionist;
import com.github.appreciated.app.layout.addons.notification.DefaultNotificationHolder;
import com.github.appreciated.app.layout.component.applayout.LeftLayouts;
import com.github.appreciated.app.layout.component.builder.AppLayoutBuilder;
import com.github.appreciated.app.layout.component.menu.left.builder.LeftAppMenuBuilder;
import com.github.appreciated.app.layout.component.menu.left.items.LeftClickableItem;
import com.github.appreciated.app.layout.component.menu.left.items.LeftNavigationItem;
import com.github.appreciated.app.layout.component.router.AppLayoutRouterLayout;
import com.github.appreciated.app.layout.entity.DefaultBadgeHolder;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.stereotype.Component;

import static com.github.appreciated.app.layout.entity.Section.FOOTER;

@Push
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@Theme(value = Lumo.class,variant = Lumo.LIGHT)
@Component @UIScope // optional but useful; allows access to this instance from views, see View1.
public class RegisteredMenuBar extends AppLayoutRouterLayout<LeftLayouts.LeftResponsive> {
    private DefaultNotificationHolder notifications = new DefaultNotificationHolder();
    private DefaultBadgeHolder badge = new DefaultBadgeHolder(5);

    public RegisteredMenuBar() {
        FindUserData findUserData = new FindUserData();
        System.out.println(findUserData.findFirstUserRoleString() +"rola w registered menu bar");
        if (findUserData.findFirstUserRoleString().equals("ROLE_USER"))
        {
            init(AppLayoutBuilder.get(LeftLayouts.LeftResponsive.class)
                    .withTitle("Przychodnia - user - pacjent")
                    .withAppMenu(LeftAppMenuBuilder.get()
                            .add(
                                    new LeftNavigationItem("Strona główna", VaadinIcon.HOME.create(), MainPage.class),
                                    new LeftNavigationItem("Zaplanowane wizyty", VaadinIcon.CALENDAR_USER.create(),
                                            RegisteredVisitsPerPatient.class),
                                    new LeftNavigationItem("Znajdź lekarza", VaadinIcon.SEARCH.create(), FormFindDoctor.class)
                            )
                            .addToSection(FOOTER, new LeftClickableItem("Zmień motyw", VaadinIcon.ADJUST.create(),
                                    clickEvent -> {
                                        ThemeChanger themeChanger = new ThemeChanger();
                                        themeChanger.changeTheme();
                                    }
                            ))
                            .addToSection(FOOTER, new LeftClickableItem("Wyloguj", VaadinIcon.EXIT.create(),
                                    clickEvent -> UI.getCurrent().getPage().setLocation("logout")
                            ))
                            .build())
                    .build());
        }

        if (findUserData.findFirstUserRoleString().equals("ROLE_ADMIN"))
        {
            init(AppLayoutBuilder.get(LeftLayouts.LeftResponsive.class)
                    .withTitle("Przychodnia - admin")
                    .withAppMenu(LeftAppMenuBuilder.get()
                            .add(
                                    new LeftNavigationItem("Strona główna", VaadinIcon.HOME.create(), MainPage.class)
//                                    new LeftNavigationItem("Kalendarz", VaadinIcon.CALENDAR_USER.create(), CalendarView.class)

                            )
                            .addToSection(FOOTER, new LeftClickableItem("Zmień motyw", VaadinIcon.ADJUST.create(),
                                    clickEvent -> {
                                        ThemeChanger themeChanger = new ThemeChanger();
                                        themeChanger.changeTheme();
                                    }
                            ))
                            .addToSection(FOOTER, new LeftClickableItem("Wyloguj", VaadinIcon.EXIT.create(),
                                    clickEvent -> UI.getCurrent().getPage().setLocation("logout")
                            ))
                            .build())
                    .build());
        }
        if (findUserData.findFirstUserRoleString().equals("ROLE_RECEPTION"))
        {
            init(AppLayoutBuilder.get(LeftLayouts.LeftResponsive.class)
                    .withTitle("Przychodnia - recepcja")
                    .withAppMenu(LeftAppMenuBuilder.get()
                            .add(
                                    new LeftNavigationItem("Strona główna", VaadinIcon.HOME.create(), MainPage.class),
//                                    new LeftNavigationItem("Kalendarz", VaadinIcon.CALENDAR_USER.create(), CalendarView.class)
                                    new LeftNavigationItem("Umów wizytę", VaadinIcon.CALENDAR.create(), FormFindDoctorReceptionist.class)

                            )
                            .addToSection(FOOTER, new LeftClickableItem("Zmień motyw", VaadinIcon.ADJUST.create(),
                                    clickEvent -> {
                                        ThemeChanger themeChanger = new ThemeChanger();
                                        themeChanger.changeTheme();
                                    }
                            ))
                            .addToSection(FOOTER, new LeftClickableItem("Wyloguj", VaadinIcon.EXIT.create(),
                                    clickEvent -> UI.getCurrent().getPage().setLocation("logout")
                            ))
                            .build())
                    .build());
        }
        if (findUserData.findFirstUserRoleString().equals("ROLE_DOCTOR"))
        {

            init(AppLayoutBuilder.get(LeftLayouts.LeftResponsive.class)
                    .withTitle("Przychodnia - doktor")
                    .withAppMenu(LeftAppMenuBuilder.get()
                            .add(
                                    new LeftNavigationItem("Strona główna", VaadinIcon.HOME.create(), MainPage.class),
                                    new LeftNavigationItem("Zaplanowane wizyty", VaadinIcon.TABLE.create(), RegisteredVisitsPerDoctor.class),
                                    new LeftNavigationItem("Zakończone wizyty",VaadinIcon.TABLE.create(),EndedVisitsPerDoctor.class)

                            )
                            .addToSection(FOOTER, new LeftClickableItem("Zmień motyw", VaadinIcon.ADJUST.create(),
                                    clickEvent -> {
                                        ThemeChanger themeChanger = new ThemeChanger();
                                        themeChanger.changeTheme();
                                    }
                            ))
                            .addToSection(FOOTER, new LeftClickableItem("Wyloguj", VaadinIcon.EXIT.create(),
                                    clickEvent -> UI.getCurrent().getPage().setLocation("logout")
                            ))
                            .build())
                    .build());
        }


    }




}
