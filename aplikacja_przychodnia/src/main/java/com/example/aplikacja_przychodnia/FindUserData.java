package com.example.aplikacja_przychodnia;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FindUserData {

    public String findCurrentlyLoggedInUserEmail() {
        String username="";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            username= currentUserName;
        }
        return username;
    }

    public Collection<? extends GrantedAuthority> findUserRoles() {
        Collection<? extends GrantedAuthority> grantedAuthorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        grantedAuthorities.forEach(o -> System.out.println(o));
        return grantedAuthorities;

    }

    public String findFirstUserRoleString() {
        List<String> userRoleList = new ArrayList<>();
        Collection<? extends GrantedAuthority> grantedAuthorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        grantedAuthorities.forEach(o -> userRoleList.add(o.toString()));
//        System.out.println(userRoleList.get(0));

        if (userRoleList.get(0).toString().equals("ROLE_DOCTOR")) System.out.println("doktor");
        return userRoleList.get(0);

    }
}
