package com.example.aplikacja_przychodnia.ui.views.tables.forDoctorView;

import com.example.aplikacja_przychodnia.DataModelAndRepo.DoctorRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.PatientRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.Visit;
import com.example.aplikacja_przychodnia.DataModelAndRepo.VisitRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomAllVisitsForDoctor;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@PageTitle("Umówione wizyty")
@Route(value = "visitsDoctor", layout = RegisteredMenuBar.class)
public class RegisteredVisitsPerDoctor  extends VerticalLayout {
    private List<Visit> visitList = new ArrayList<>();

    @Autowired
    public RegisteredVisitsPerDoctor(VisitRepository visitRepository, DoctorRepository doctorRepository, PatientRepository patientRepository) {
        add(new H1("Zaplanowane wizyty"));
        add(new H3("Kliknij na wizytę, aby edytować"));

        FindUserData findUserData = new FindUserData();
        String currentlyLoggedInUserEmail = findUserData.findCurrentlyLoggedInUserEmail();
//        System.out.println(currentlyLoggedInUserEmail);
        List<CustomAllVisitsForDoctor> customAllRegisteredVisitsLists = new ArrayList<>();
        visitList = visitRepository.findAllVisitsForDoctor(currentlyLoggedInUserEmail,false);
//        System.out.println(visitList.toString());
        if (!visitList.isEmpty()) {
            visitList.forEach(visit -> {
                customAllRegisteredVisitsLists.add(new CustomAllVisitsForDoctor(visit.getVisitDateTime(),
                        visit.getPatient().getUser().getFirstName(),visit.getPatient().getUser().getLastName(),
                        visit.getDescription(),visit.getPatient().getUser().getId(),visit.isFinished()));
            });

            Grid<CustomAllVisitsForDoctor> visitGrid = new Grid<>();
            visitGrid.setItems(customAllRegisteredVisitsLists);
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomVisitDate).setHeader("Data wizyty");
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomVisitTime).setHeader("Godzina wizyty");
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomPatientFormatted).setHeader("Pacjent");

            visitGrid.addItemClickListener(event -> {
//                Notification.show(event.getItem().toString(),1000, Notification.Position.MIDDLE);
                System.out.println(event.getItem().getVisitDateTime()+" | userEmail="+event.getItem().getUserId()
                        +" | doctorEmail="+currentlyLoggedInUserEmail);

                new DialogAddVisitDescription(event.getItem().getVisitDateTime(),event.getItem().getUserId(),currentlyLoggedInUserEmail,visitRepository,doctorRepository,event.getItem().getDescription(),patientRepository,event.getItem().isFinished()).open();
            });

            add(visitGrid);
        }
    }
}
