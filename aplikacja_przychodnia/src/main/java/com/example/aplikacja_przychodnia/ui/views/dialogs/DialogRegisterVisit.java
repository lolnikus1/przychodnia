package com.example.aplikacja_przychodnia.ui.views.dialogs;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.service.VisitService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Transactional
public class DialogRegisterVisit extends Dialog {
    private DatePicker visitDate;
    private TimePicker visitTime;

    public DialogRegisterVisit(boolean newInstance, Long doctorId, VisitService visitService, DoctorRepository doctorRepository, PatientRepository patientRepository , VisitRepository visitRepository, UserRepository userRepository) {

        setCloseOnEsc(true);
        setCloseOnOutsideClick(true);
        setWidthFull();

        VerticalLayout layout = new VerticalLayout();
        layout.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.STRETCH);
        layout.setSizeFull();

//        TextField textField = new TextField("Id");
//        textField.setWidth("400px");
//        if (!doctorId.toString().isEmpty()) textField.setValue(doctorId.toString());
//        add(textField);

        visitDate = new DatePicker("Data wizyty");
        visitTime = new TimePicker("Godzina wizyty");
        Button register = new Button("Potwierdź");



        register.addClickListener(click -> {

            FindUserData findUserData = new FindUserData();
            User user = userRepository.findByEmail(findUserData.findCurrentlyLoggedInUserEmail());
            System.out.println(user.toString()+ "| "+findUserData.findCurrentlyLoggedInUserEmail()+ "| "+user.getId());


//            Patient patient = patientRepository.getOne(1L);
            Patient patient = patientRepository.findPatientByUserId(user.getId());
            Doctor doctor = doctorRepository.findDoctorById(doctorId);

            System.out.println("-------------------------------------------------------------------");
            System.out.println(patient.toString());
//
            System.out.println(doctor.toString());



            if(!DoctorIsBusy(visitRepository,doctor,visitDate,visitTime)){
                registerVisit(visitService, patient,doctor);
                Notification.show("Zapisano na wizytę dnia "+visitDate.getValue()+" o godz. "+visitTime.getValue(),3000, Notification.Position.MIDDLE);
            }
            else Notification.show("Ktoś już jest zapisany na ten termin...",3000, Notification.Position.MIDDLE);
        });
        VerticalLayout v  = new VerticalLayout();
        v.add(visitDate,visitTime,register);
        add(v);
        visitDate.setI18n(
                new DatePicker.DatePickerI18n().setWeek("tydzień").setCalendar("kalendarz")
                        .setClear("Wyczyść").setToday("dzisiaj")
                        .setCancel("Anuluj").setFirstDayOfWeek(1)
                        .setMonthNames(Arrays.asList("styczeń", "luty",
                                "marzec", "kwiecień", "maj", "czerwiec",
                                "lipiec", "sierpień", "wrzesień", "październik",
                                "listopad", "grudzień")).setWeekdays(
                        Arrays.asList("niedziela", "poniedziałek", "wtorek",
                                "środa", "czwartek", "piątek",
                                "sobota")).setWeekdaysShort(
                        Arrays.asList("nd.", "pon.", "wt.", "śr.", "czw.", "pt.",
                                "sob.")));
    }

    private boolean DoctorIsBusy(VisitRepository visitRepository, Doctor doctor, DatePicker visitDate, TimePicker visitTime) {
        List<Visit> visits = visitRepository.getAllByDoctorAndVisitDateTime(doctor,
                ConvertLocalDateTimeToDate(visitDate.getValue().atTime(visitTime.getValue())));
        return visits.size() != 0;
    }

    private void registerVisit(VisitService visitService, Patient patient, Doctor doctor) {
//        System.out.println(patient.toString());
        System.out.println(doctor.toString());
        Visit v = new Visit();
        v.setPatient(patient);
        v.setDoctor(doctor);
        Date visitDateTime = ConvertLocalDateTimeToDate(visitDate.getValue().atTime(visitTime.getValue()));
        v.setVisitDateTime(visitDateTime);
        visitService.addVisit(v);
    }

    private Date ConvertLocalDateTimeToDate(LocalDateTime atTime) {
        return Date.from(atTime.atZone(ZoneId.systemDefault()).toInstant());
    }


}
