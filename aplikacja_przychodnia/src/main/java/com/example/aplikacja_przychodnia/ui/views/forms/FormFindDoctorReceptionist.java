package com.example.aplikacja_przychodnia.ui.views.forms;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomDoctorObject;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.example.aplikacja_przychodnia.service.VisitService;
import com.example.aplikacja_przychodnia.ui.views.dialogs.DialogRegisterVisit;
import com.example.aplikacja_przychodnia.ui.views.dialogs.DialogRegisterVisitByReceptionist;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@PageTitle("Znajdź lekarza")
@Route(value = "findDoctorReceptionist", layout = RegisteredMenuBar.class)
public class FormFindDoctorReceptionist extends VerticalLayout {
    Grid<CustomDoctorObject> grid;
    ComboBox<String> lastName,specialization;

    @Autowired
    public FormFindDoctorReceptionist(DoctorRepository doctorRepository, VisitService visitService, PatientRepository patientRepository , VisitRepository visitRepository, UserRepository userRepository){
        lastName = new ComboBox<>("Nazwisko");
        lastName.setClearButtonVisible(true);
        specialization = new ComboBox<>("Specjalizacja");
        specialization.setClearButtonVisible(true);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        fillCBLastName(doctorRepository,lastName);
        horizontalLayout.add(lastName);
//        add(lastName);
        fillCBSpecialization(doctorRepository,specialization);
        horizontalLayout.add(specialization);
//        add(specialization);
        add(horizontalLayout);
        Button findDoctor = new Button("Szukaj");
        findDoctor.addClickListener(click -> {
            if(lastName.getValue()!=null){
                remove(grid);
                if(specialization.getValue()!=null){
                    fillTableByDoctorLastNameAndSpecialization(doctorRepository,specialization.getValue());
                } else {
                    fillTableByDoctorLastName(doctorRepository);
                }
                add(grid);
            } else if (specialization.getValue()!=null){
                remove(grid);
                fillTableByDoctorSpecialization(doctorRepository);
                add(grid);
            } else { Notification.show("Nie wybrano lekarza",3000, Notification.Position.MIDDLE); }
        });
        findDoctor.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        add(findDoctor);
        grid = new Grid<>(CustomDoctorObject.class);
        fillTableWithAllExistingDoctors(doctorRepository);
        grid.removeColumnByKey("doctorId");
        grid.addComponentColumn(doctor -> {
            Button button = new Button("Umów na wizytę");
            button.addClickListener(click ->{
//                UI.getCurrent().getPage().setLocation("registerVisit?doctorid="+doctor.getDoctorId()+"&patientid=1");

                new DialogRegisterVisitByReceptionist(true,doctor.getDoctorId(),visitService,doctorRepository,patientRepository,visitRepository,userRepository).open();

                //Notification.show(Long.toString(doctor.getDoctorId()),1000, Notification.Position.BOTTOM_START);
            });
            return button;
        }).setHeader("");
        add(grid);
    }

    private void fillTableByDoctorLastNameAndSpecialization(DoctorRepository doctorRepository, String value) {
        List<Doctor> doctorsByLastName = doctorRepository.findDoctorsByLastName(lastName.getValue());
        doctorsByLastName.removeIf(d -> !d.getSpecialization().equals(value));

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        doctorsByLastName.forEach(doctor ->
                customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName())));

        if (!customDoctorObjects.isEmpty()) {
            grid.setItems(customDoctorObjects);
        }
        else { Notification.show("Nie ma takiego lekarza",1000, Notification.Position.MIDDLE);}
    }

    private void fillTableByDoctorSpecialization(DoctorRepository doctorRepository) {
        List<Doctor> doctorSpecializationsAndTitle = doctorRepository.findDoctorsBySpecialization(specialization.getValue());

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        doctorSpecializationsAndTitle.forEach(doctor ->
                customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName())));

        if (!customDoctorObjects.isEmpty()) {
            grid.setItems(customDoctorObjects);
//            grid.addColumn(new ComponentRenderer<>(doctor -> new Button("Umów się na wizytę"))).setHeader("");
        }
        else { Notification.show("Nie ma takiego lekarza",1000, Notification.Position.MIDDLE); }
    }

    private void fillTableByDoctorLastName(DoctorRepository doctorRepository){
        List<Doctor> doctorsByLastName = doctorRepository.findDoctorsByLastName(lastName.getValue());

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        doctorsByLastName.forEach(doctor ->
                customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName())));

        if (!customDoctorObjects.isEmpty()) {
            grid.setItems(customDoctorObjects);
        }
        else { Notification.show("Nie ma takiego lekarza",1000, Notification.Position.MIDDLE);}
    }

    private void fillTableWithAllExistingDoctors(DoctorRepository doctorRepository) {
        List<Doctor> listAllExistingDoctors = doctorRepository.findAllDoctors();
        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        listAllExistingDoctors.forEach(doctor -> {
            customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName()));});
        grid.setItems(customDoctorObjects);
    }

    private void fillCBLastName(DoctorRepository doctorRepository, ComboBox<String> lastName) {
        List<String> surnames = new ArrayList<>();
        List<User> userSecondNameList = doctorRepository.findAllUsernames();
        System.out.println(userSecondNameList.toString());
        userSecondNameList.forEach(user -> {
//            String aaaaa = user.getLastName();
//            System.out.println(user.getLastName());
            if (!user.getLastName().isEmpty()) surnames.add(user.getLastName());
        });
        if(!surnames.isEmpty()) lastName.setItems(surnames);
    }

    private void fillCBSpecialization(DoctorRepository doctorRepository, ComboBox<String> specialization) {
        List<String> specialzationList = doctorRepository.findAllSpecializations();
        specialization.setItems(specialzationList);
    }
}

