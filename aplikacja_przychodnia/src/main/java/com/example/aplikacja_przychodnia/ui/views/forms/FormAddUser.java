package com.example.aplikacja_przychodnia.ui.views.forms;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.navAndThemes.NonRegisteredMenuBar;
import com.example.aplikacja_przychodnia.service.DoctorService;
import com.example.aplikacja_przychodnia.service.PatientService;
import com.example.aplikacja_przychodnia.service.UserService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.print.Doc;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@PageTitle("Dodawanie usera")
@Route(value = "formUser", layout = NonRegisteredMenuBar.class)
public class FormAddUser extends VerticalLayout {

    //wybrany typ użytkownika z listy (kolumna description)
    private String chosenUserTypeDescription;

    // znalezniona w bazie rola użytkownika w kolumnie role - na podstawie chosenUserTypeDescription
    private String foundUserRole;

    private TextField firstName,lastName;
    private ComboBox<String> profTitle, specialization;
    private EmailField email;
    private PasswordField password;
    private Binder<User> binder;



    @Autowired
    public FormAddUser(UserService userService, UserRoleRepository userRoleRepository, DoctorService doctorService, PatientService patientService,SpecializationRepository specializationRepository) {
        String width= "350px";

        firstName = new TextField("Imię");
        firstName.setWidth(width);
        lastName = new TextField("Nazwisko");
        lastName.setWidth(width);

        profTitle = new ComboBox<>("Tytuł zawodowy");
        profTitle.setItems("dr","prof. dr med.","dr med.","mgr");
        profTitle.setWidth(width);

        specialization = new ComboBox<>("Specjalizacja");
        List<String> specializationList = specializationRepository.findAllSpecializations();
        if (!specializationList.isEmpty()) specialization.setItems(specializationList);

        specialization.setWidth(width);
        profTitle.setVisible(false);
        specialization.setVisible(false);

        email = new EmailField("Email");
        email.setWidth(width);
        email.setClearButtonVisible(true);
        email.setErrorMessage("Podaj prawidłowy adres email");

        H2 addInfo = new H2("Dodatkowe informacje");
        addInfo.setVisible(false);
        password = new PasswordField("Hasło");
        password.setWidth(width);

        if (!specializationList.isEmpty()) add(firstName,lastName,email,password,addInfo,profTitle,specialization);
        if (specializationList.isEmpty()) {
            Label fieldErrorNoUserTypes =
                new Label("Błąd wczytywania danych. Skontakuj się z administratorem.");
            add(firstName,lastName,email,password,addInfo,profTitle,fieldErrorNoUserTypes);
        }


        RadioButtonGroup<String> radioButtonGroup = new RadioButtonGroup<>();

        //pobieranie z bazy danych typów użytkownika do radio button z wyjątkiem admina
        generateRadioButtons(userRoleRepository,radioButtonGroup);
        add(radioButtonGroup);

        radioButtonGroup.addValueChangeListener(event -> {
                    chosenUserTypeDescription =event.getValue();
                    foundUserRole=userRoleRepository.findUserRoleByDescription(chosenUserTypeDescription);
                    if(foundUserRole.equals("ROLE_DOCTOR")){
                        addInfo.setVisible(true);
                        profTitle.setVisible(true);
                        specialization.setVisible(true);
                    } else {
                        addInfo.setVisible(false);
                        profTitle.setVisible(false);
                        specialization.setVisible(false);
                    }
                    Notification.show(chosenUserTypeDescription +" | "+foundUserRole);
                }
        );

        //

        Button save = new Button("Dodaj");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button cancel = new Button("Anuluj");
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        HorizontalLayout buttons = new HorizontalLayout(save, cancel);
        add(buttons);

        binder = new Binder<>(User.class);
        User user = new User();
        defineFormValidation(user);

        save.addClickListener(event -> {
            try {
                binder.validate();
                if (foundUserRole!=null && profTitle!=null && specialization!=null && binder.isValid()) {
//                    User user = new User();
                    user.setFirstName(firstName.getValue());
                    user.setLastName(lastName.getValue());
                    user.setEmail(email.getValue());
                    user.setPassword(password.getValue());
                    userService.addWithDefinedRole(user,foundUserRole);
                    if(foundUserRole.equals("ROLE_DOCTOR")){
                        Doctor doctor = new Doctor(user,profTitle.getValue(),specialization.getValue());
                        doctorService.addDoctor(doctor);
                    } else if(foundUserRole.equals("ROLE_USER")){
                        Patient patient = new Patient(user);
                        patientService.addPatient(patient);
                    }
                    Notification.show("Użytkownik dodany!",1000, Notification.Position.MIDDLE);
                }
                else Notification.show("Nie wypełniono wszystkich pół formularza lub wprowadzone dane są niepoprawne!",1000, Notification.Position.MIDDLE);

            } catch (Exception e) {
                e.printStackTrace();
                Notification.show("Użytkownik o podanych danych już istnieje!",1000, Notification.Position.MIDDLE);
            }
        });

        cancel.addClickListener(event -> UI.getCurrent().navigate("/loggedOutMainPage"));





    }

    //sprawdza poprawność pierwszych 4 pól w formularzu
    private void defineFormValidation(User user) {
        //minimum 1 cyfra, minimum 1 małą litera, minimum 1 duża, minimum 1 znak specjalny,bez pustych znaków, minimum 8 znaków
//        String patternPassword = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        //wersja bez znaków specjalnych
        String patternPassword = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        String patternNames = "^[^\\d\\s]+$";



        binder.forField(firstName)
                .asRequired("Pole wymagane.")
                .withValidator((value, context) -> {
                    if (!value.matches(patternNames)) return ValidationResult.error(
                            "Pole nie może zawierać spacji ani cyfr");
                    else return ValidationResult.ok();
                })
                .bind(User::getFirstName, User::setFirstName);

        binder.forField(lastName)
                .asRequired("Pole wymagane.")
                .withValidator((value, context) -> {
                    if (!value.matches(patternNames)) return ValidationResult.error(
                            "Pole nie może zawierać spacji ani cyfr");
                    else return ValidationResult.ok();
                })
                .bind(User::getLastName, User::setLastName);

        binder.forField(email)
                .asRequired("Pole wymagane.")
                .withValidator(new EmailValidator("Niepoprawna składnia adresu email"))
                .bind(User::getEmail,User::setEmail);
        binder.forField(password)
                .asRequired("Pole wymagane.")
                .withValidator((value, context) -> {
                    if (!value.matches(patternPassword)) return ValidationResult.error(
                            "Hasło musi mieć długość minimum 8 znaków bez spacji, a także zawierać minimum: jedną dużą literę, " +
                                    "jedną małą literę, jedną cyfrę. ");
                    else return ValidationResult.ok();
                })
                .bind(User::getPassword,User::setPassword);
        binder.setBean(user);
    }

    private void generateRadioButtons(UserRoleRepository userRoleRepository,RadioButtonGroup<String> group) {
        List<UserRole> userTypes = userRoleRepository.findAll();
        List<String> listaRozwijana = new ArrayList<>();

        for (UserRole userType : userTypes) {
            if (!userType.getDescription().equals("admin")){ listaRozwijana.add(userType.getDescription()); }}
        group.setItems(listaRozwijana);
    }
}
