package com.example.aplikacja_przychodnia.ui.views.tables.forDoctorView;

import com.example.aplikacja_przychodnia.DataModelAndRepo.DoctorRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.PatientRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.Visit;
import com.example.aplikacja_przychodnia.DataModelAndRepo.VisitRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomAllVisitsForDoctor;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.apache.coyote.http11.Http11AprProtocol;

import java.util.ArrayList;
import java.util.List;

@PageTitle("Zakończone wizyty")
@Route(value = "visitsDoctorEnded", layout = RegisteredMenuBar.class)
public class EndedVisitsPerDoctor extends VerticalLayout {
    private List<Visit> visitList = new ArrayList<>();

    public EndedVisitsPerDoctor(VisitRepository visitRepository, PatientRepository patientRepository, DoctorRepository doctorRepository) {
        add(new H1("Zakończone wizyty"));
        FindUserData findUserData = new FindUserData();
        String currentlyLoggedInUserEmail = findUserData.findCurrentlyLoggedInUserEmail();
        System.out.println(currentlyLoggedInUserEmail);
        List<CustomAllVisitsForDoctor> customAllRegisteredVisitsLists = new ArrayList<>();
        visitList = visitRepository.findAllVisitsForDoctor(currentlyLoggedInUserEmail,true);

        System.out.println(visitList.toString());
        if (!visitList.isEmpty()) {
            visitList.forEach(visit -> {
                customAllRegisteredVisitsLists.add(new CustomAllVisitsForDoctor(visit.getVisitDateTime(),
                        visit.getPatient().getUser().getFirstName(),
                        visit.getPatient().getUser().getLastName(),
                        visit.getDescription(),
                        visit.getPatient().getUser().getId(),
                        visit.isFinished()

                ));
            });

            Grid<CustomAllVisitsForDoctor> visitGrid = new Grid<>();
            visitGrid.setItems(customAllRegisteredVisitsLists);
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomVisitDate).setHeader("Data wizyty");
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomVisitTime).setHeader("Godzina wizyty");
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomPatientFormatted).setHeader("Pacjent");

            visitGrid.addItemClickListener(event -> {

                new DialogAddVisitDescription(event.getItem().getVisitDateTime(), event.getItem().getUserId(),
                        currentlyLoggedInUserEmail,visitRepository,doctorRepository,
                        event.getItem().getDescription(),patientRepository,event.getItem().isFinished()).open();
            });

            add(visitGrid);
        }



    }
}
