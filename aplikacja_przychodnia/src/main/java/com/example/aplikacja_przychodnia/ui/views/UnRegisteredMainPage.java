package com.example.aplikacja_przychodnia.ui.views;


import com.example.aplikacja_przychodnia.navAndThemes.NonRegisteredMenuBar;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;


@PageTitle("Strona główna")
@Route(value = "loggedOutMainPage", layout = NonRegisteredMenuBar.class)

public class UnRegisteredMainPage extends VerticalLayout {


    @Autowired
    public UnRegisteredMainPage() {
        H1 h1 = new H1("Witamy na stronie głównej");
        add(h1);

    }


}
