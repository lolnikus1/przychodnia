package com.example.aplikacja_przychodnia.ui.views.tables.forPatientView;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomAllRegisteredVisitsList;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@PageTitle("Znajdź lekarza")
@Route(value = "registeredVisitsCurrentPatient", layout = RegisteredMenuBar.class)
public class RegisteredVisitsPerPatient extends VerticalLayout {
//    Grid<CustomDoctorObject> grid;
//    ComboBox<String> lastName,specialization;
    List<Visit> visitList = new ArrayList<>();

    @Autowired
    public RegisteredVisitsPerPatient(DoctorRepository doctorRepository, VisitRepository visitRepository){
        add(new H2("Nadchodzące wizyty"));

        FindUserData findUserData = new FindUserData();
        String currentlyLoggedInUserEmail = findUserData.findCurrentlyLoggedInUserEmail();
        System.out.println(currentlyLoggedInUserEmail + " adres email");
        List<CustomAllRegisteredVisitsList> customAllRegisteredVisitsLists = new ArrayList<>();
        visitList = visitRepository.findAllVisitsForUser(currentlyLoggedInUserEmail);
        System.out.println(visitList+ " lista wizyt przed opakowaniem w custom pojo");
        if (!visitList.isEmpty()) {
            visitList.forEach(visit -> {
                customAllRegisteredVisitsLists.add(new CustomAllRegisteredVisitsList(visit.getVisitDateTime(),
                        visit.getDoctor().getProfTitle(),visit.getDoctor().getUser().getFirstName(),
                        visit.getDoctor().getUser().getLastName(),
                        visit.getDoctor().getSpecialization()
                ));
            });

            Grid<CustomAllRegisteredVisitsList> visitGrid = new Grid<>();
            visitGrid.setItems(customAllRegisteredVisitsLists);
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getCustomVisitDate).setHeader("Data wizyty");
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getCustomVisitTime).setHeader("Godzina wizyty");
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getDoctorWithTitle).setHeader("Lekarz");
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getSpecialization).setHeader("Specjalizacja");;



            add(visitGrid);

        }
        else add(new H2("Nie ma jeszcze zaplanowanych wizyt"));
        System.out.println(customAllRegisteredVisitsLists.toString());



    }

}
