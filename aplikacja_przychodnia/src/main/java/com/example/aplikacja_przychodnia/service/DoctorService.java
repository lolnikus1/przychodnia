package com.example.aplikacja_przychodnia.service;

import com.example.aplikacja_przychodnia.DataModelAndRepo.Doctor;
import com.example.aplikacja_przychodnia.DataModelAndRepo.DoctorRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.User;
import com.example.aplikacja_przychodnia.DataModelAndRepo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorService {

    private UserRepository userRepository;
    private DoctorRepository doctorRepository;

    public DoctorService(){
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setDoctorRepository(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public void addDoctor(Doctor doctor){
        doctorRepository.save(doctor);
    };
}
