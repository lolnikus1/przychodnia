package com.example.aplikacja_przychodnia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.aplikacja_przychodnia.DataModelAndRepo.User;
import com.example.aplikacja_przychodnia.DataModelAndRepo.UserRole;
import com.example.aplikacja_przychodnia.DataModelAndRepo.UserRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.UserRoleRepository;


@Service
public class UserService {

    private static final String DEFAULT_ROLE = "ROLE_USER";
    private UserRepository userRepository;
    private UserRoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRoleRepository(UserRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void addWithDefinedRole(User user, String userRole) {
        UserRole defaultRole = roleRepository.findByRole(userRole);
        user.getRoles().add(defaultRole);
        String passwordHash = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwordHash);
        userRepository.save(user);
    }
}